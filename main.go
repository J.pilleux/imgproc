package main

import (
	"flag"
	"fmt"
	"os"
	"time"
	"udemy/imgproc/filter"
	"udemy/imgproc/task"
)

func main() {
	var srcDir = flag.String("src", "", "Input directory")
	var dstDir = flag.String("dst", "", "Outut directory")
	var filterType = flag.String("filter", "grayscale", "grayscale/blur")
	var taskType = flag.String("task", "channels", "channels/waitgroup")
	var poolsize = flag.Int("poolsize", 4, "Number of workers to use for the channels task")
	flag.Parse()

	var f filter.Filter
	switch *filterType {
	case "grayscale":
		f = &filter.Greyscale{}
	case "blur":
		f = &filter.Blur{}
	default:
		fmt.Fprintf(os.Stderr, "Unknown filter type: %v\n", *filterType)
		os.Exit(1)
	}

	var t task.Tasker
	switch *taskType {
	case "channels":
		t = task.NewChanTask(*srcDir, *dstDir, f, *poolsize)
	case "waitgroup":
		t = task.NewWaitGroupTask(*srcDir, *dstDir, f)
	default:
		fmt.Fprintf(os.Stderr, "Unknown task type: %v\n", *taskType)
		os.Exit(1)
	}

	start := time.Now()
	t.Process()
	elapsed := time.Since(start)
	fmt.Printf("Image processing took: %fs\n", elapsed.Seconds())
}
